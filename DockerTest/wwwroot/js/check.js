﻿// Write your Javascript code.
$(document).ready(function () {
    checkPubsub.init();
});
var checkPubsub = {
    init: function () {
        if ($("#completed").val() == "0") {
            this.getStatus();
        }
    },
    getStatus: function () {
        var check = $.get({
            url: "/Home/Success/" + $("#id").val()
        });
        check.done(function (result) {
            $("#loading").hide();
            if (result == "Unregistered") {
                window.location.href = "/Home/Recreate" + $("#id").val();
            } else {
                $("#success").text(result);
                $("#success").show();
                $("#loading").hide();
            }
        });
    }
}