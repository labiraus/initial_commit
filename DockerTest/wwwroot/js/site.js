﻿webshim.setOptions('forms', {
    lazyCustomMessages: true,
    replaceValidationUI: true,
    addValidators: true
});

(function () {
    var stateMatches = {
        'true': true,
        'false': false,
        'auto': 'auto'
    };
    var enhanceState = (location.search.match(/enhancelist\=([true|auto|false|nopolyfill]+)/) || ['', 'auto'])[1];

    webshim.ready('jquery', function () {
        $(function () {
            $('.polyfill-type select')
                .val(enhanceState)
                .on('change', function () {
                    location.search = 'enhancelist=' + $(this).val();
                })
                ;
        });
    });
    webshim.setOptions('forms', {
        customDatalist: stateMatches[enhanceState]
    });
    webshim.setOptions('forms-ext', {
        replaceUI: stateMatches[enhanceState]
    });
})();


// load the forms polyfill
webshim.polyfill('forms forms-ext');