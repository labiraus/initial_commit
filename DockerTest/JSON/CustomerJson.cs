﻿using DockerTest.Model;
using System;

namespace DockerTest.JSON
{
    public class CustomerJson
    {
        public CustomerJson()
        {

        }
        public CustomerJson(Customer customer)
        {
            firstName = customer.FirstName;
            lastName = customer.LastName;
            dateOfBirth = customer.DateOfBirth;
        }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime dateOfBirth { get; set; }
    }
}
