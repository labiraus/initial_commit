﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DockerTest.JSON
{
    public class ResponseJson
    {
        public string decisionResult { get; set; }
        public bool Accepted()
        {
            return decisionResult == "Accepted";
        }
    }
}
