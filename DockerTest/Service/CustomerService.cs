﻿using DockerTest.JSON;
using DockerTest.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DockerTest.Service
{
    public class CustomerService : ICustomerService
    {
        private readonly MyContext _context;
        private readonly HttpClient _client;

        private readonly Dictionary<Guid, Task> runningTasks;
        private readonly CancellationTokenSource tokenSource;
        public CustomerService(MyContext context, HttpClient client)
        {
            _context = context;
            _client = client;
            tokenSource = new CancellationTokenSource();
            runningTasks = new Dictionary<Guid, Task>();
        }

        public async Task Create(Customer customer)
        {
            await _context.Customers.AddAsync(customer);
            var task = run(customer);
            runningTasks[customer.Id] = task;
        }

        public async Task<Customer> GetStatus(Guid customerId)
        {
            return await _context.Customers.FindAsync(customerId);
        }

        public async Task<string> Success(Guid customerId)
        {
            if (await _context.Customers.FindAsync(customerId) == null)
                return "Unregistered";
            var task = runningTasks[customerId];
            if (task != null)
                task.Wait();
            var customer = await _context.Customers.FindAsync(customerId);
            return customer.Status.ToString();
        }

        private Task run(Customer customer)
        {
            return Task.Run(async () =>
            {
                var cust = await _context.Customers.FindAsync(customer.Id);
                if (cust == null)
                {
                    cust = customer;
                    await _context.Customers.AddAsync(customer);
                    await _context.SaveChangesAsync();
                }
                bool completed = false;
                while (!completed)
                {
                    var attempt = new Attempt()
                    {
                        Customer = cust,
                        SendTime = DateTime.Now,
                        StatusId = Status.Sent
                    };
                    cust.Status = Status.Sent;
                    await _context.SaveChangesAsync();
                    try
                    {
                        var json = JsonConvert.SerializeObject(new CustomerJson(cust));
                        var payload = new StringContent(json, Encoding.UTF8, "application/json");
                        var httpResponse = await _client.PostAsync("http://dummydecisionengine.azurewebsites.net/decision", payload);
                        if (httpResponse.StatusCode != HttpStatusCode.OK)
                        {
                            throw new Exception(httpResponse.ReasonPhrase);
                        }
                        var responseContent = await httpResponse.Content.ReadAsStringAsync();

                        var response = JsonConvert.DeserializeObject<ResponseJson>(responseContent);
                        attempt.StatusId = response.Accepted() ? Status.Accepted : Status.Declined;
                        cust.Status = attempt.StatusId;
                        completed = true;
                    }
                    catch (Exception e)
                    {
                        attempt.StatusId = Status.Error;
                        attempt.ErrorMessage = e.Message;
                    }
                    await _context.SaveChangesAsync();
                }
            },
            tokenSource.Token);
        }
    }
}
