﻿using DockerTest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DockerTest.Service
{
    public interface ICustomerService
    {
        Task Create(Customer customer);
        Task<string> Success(Guid customerId);
        Task<Customer> GetStatus(Guid customerId);
    }
}
