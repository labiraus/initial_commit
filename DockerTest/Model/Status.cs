﻿namespace DockerTest.Model
{
    public enum Status
    {
        Created,
        Sent,
        Accepted,
        Declined,
        Error
    }
}
