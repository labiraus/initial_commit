﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DockerTest.Model
{
    public class Customer
    {
        public Customer()
        {
            Id = Guid.NewGuid();
            Status = Status.Created;
            Attempts = new List<Attempt>();
        }
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }
        public Status Status { get; set; }
        public ICollection<Attempt> Attempts { get; set; }
        public bool Completed
        {
            get { return Status == Status.Accepted || Status == Status.Declined; }
        }
    }
}
