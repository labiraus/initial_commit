﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DockerTest.Model
{
    public class Attempt
    {
        public int Id { get; set; }
        public virtual Customer Customer { get; set; }
        public DateTime SendTime { get; set;}
        public Status StatusId { get; set; }
        public string ErrorMessage { get; set; }
    }
}
