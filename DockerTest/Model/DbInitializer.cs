﻿using System;
using System.Collections.Generic;

namespace DockerTest.Model
{
    public static class DbInitializer
    {
        public static void Initialize(MyContext context)
        {
            try
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                var customer = new Customer()
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Fred",
                    LastName = "Flintstone",
                    DateOfBirth = new DateTime(1987, 8, 1),
                    Attempts = new List<Attempt>()
                };
                context.Customers.Add(customer);
                context.SaveChanges();
            }
            catch (Exception e)
            {

            }
        }
    }
}
