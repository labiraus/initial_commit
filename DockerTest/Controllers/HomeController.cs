﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DockerTest.Model;
using DockerTest.Service;

namespace DockerTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICustomerService _customerService;

        public HomeController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Check(Guid id)
        {
            var customer = await _customerService.GetStatus(id);

            if (customer == null)
                return RedirectToAction("Recreate", new { id = id });

            return View(customer);
        }

        public IActionResult Recreate(Guid id)
        {
            if (id == null)
                id = Guid.NewGuid();

            return View(new Customer() { Id = id });
        }

        [HttpPost]
        public async Task<IActionResult> Create(Customer customer)
        {
            await _customerService.Create(customer);
            return RedirectToAction("Check", new { id = customer.Id });
        }

        [HttpGet]
        public async Task<string> Success(Guid id)
        {
            return await _customerService.Success(id);
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
